#!/bin/bash

# For ttuner-processed ab1 files with too long IDs, from a specific provider.
# This script is not intended for any kind of general use.

for f in *.ab1.fastq; do
    echo $f
    # 150430IGK3B1SP6
    preID=${f%%_*} # get the first part of the name, before the first _
    # IGIGKBAC3B1-SP6.ab1.fastq
    postID=${f##*_}
    # remove trailing extensions, IGIGKBAC3B1-SP6
    postID_noext=${postID%.ab1.fastq}

    # Perform in-place replace.
    sed --in-place=_orig "s/^@${f%.fastq}/@${postID_noext}/" $f
    # Rename file to remove the unnecessary .ab1 from .ab1.fastq.
    mv $f ${f/.ab1/}
done
