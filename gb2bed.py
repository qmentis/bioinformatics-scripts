#!/usr/bin/env python
# encoding: utf-8
"""
based on: https://github.com/DRL/bioinformatic_tools/blob/master/gb2bed.py
requires:  biopython
"""

from __future__ import print_function
import os
import sys
from Bio import SeqIO


def gb2bed(infile):
    out_fh = open("%s.bed" % (os.path.splitext(infile)[0]), 'w')
    for record in SeqIO.parse(open(infile, "rU"), "genbank") :
        #print record.__dict__
        chrom = record.name
        for feature in record.features:
            #print feature.__dict__
            if feature.type in ['cluster']:
                start = feature.location.start.position
                stop = feature.location.end.position
                number = feature.qualifiers['note'][0].split(':')[1].strip()
                name = number + '_' + feature.qualifiers['product'][0]
                if feature.strand < 0:
                    strand = "-"
                else:
                    strand = "+"
                bed_line = "%s\t%s\t%s\t%s\t%s\t%s\n" % (chrom, start, stop, name, 0, strand)
                out_fh.write(bed_line)
    out_fh.close()


if __name__ == '__main__':
    infile = ''
    try:
        infile = sys.argv[1]
    except:
        sys.exit("gb2bed.py INFILE")
    gb2bed(infile)
