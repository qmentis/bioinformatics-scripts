#!/usr/bin/python

'''
abi2fastq: convert ab1 files to fastq
Can be used both as a standalone program and an importable module.
Does not add quality values if they are missing from the ab1 file; for that,
use the ttuner tool.
Does not overwrite existing output files.
'''


from __future__ import print_function
import sys
from os.path import exists, splitext
from Bio import SeqIO
from Bio.Alphabet import generic_dna


def abi2fastq(infile, outfile = ''):
    if outfile == '':
        outfile = splitext(infile)[0] + '.fastq'
    if exists(outfile):
        print('%s exists, not overwriting!' % outfile, file = sys.stderr)
    else:
        SeqIO.convert(infile, "abi", outfile, "fastq", generic_dna)
    return outfile

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('1 argument is required: input_file.ab1')
        print('Output file will be written to the same filename but with the .fna extension.')
        sys.exit(2)
    else:
        abi2fastq(sys.argv[1])
