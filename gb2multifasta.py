#!/usr/bin/python
# see also
# http://www2.warwick.ac.uk/fac/sci/moac/people/students/peter_cock/python/genbank2fasta/

from __future__ import print_function
import sys
from Bio import SeqIO


def convert(infile, outfile):
    counter = 1
    in_handle = open(infile)
    out_handle = open(outfile, "w")
    for seq_record in SeqIO.parse(in_handle, "genbank") :
        print("Dealing with GenBank record %s" % seq_record.id)
        for seq_feature in seq_record.features :
            # print help(seq_feature.extract)
            out_handle.write(">%s from %s\n%s\n" % (
                   # seq_feature.id,
                   counter,
                   seq_record.name,
                   seq_feature.extract(seq_record.seq)))
            counter += 1
    out_handle.close()
    in_handle.close()


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('2 arguments required: infile, outfile')
        sys.exit(1)
    else:
        convert(sys.argv[1], sys.argv[2])
