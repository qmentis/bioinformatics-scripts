#!/usr/bin/python

# Given a GenBank input file, print counts of all encountered feature types,
# and a total number of features in the file.


import sys
import os
from Bio import SeqIO


def count(infile):
    print
    print 'Note: "pseudo_qualifier" counter lists the number of features'
    print 'which had a "pseudo" qualifier; it is not a feature type.'
    print
    in_handle = open(infile)
    for seq_record in SeqIO.parse(in_handle, "genbank"):
        print "Processing GenBank record %s:" % seq_record.id
        # Dictionary of simple integer counters for all feature types;
        # 'pseudo_qualifier' is special and thus is pre-defined.
        counters = {'pseudo_qualifier': 0}
        for seq_feature in seq_record.features:
            if seq_feature.type not in counters:
                counters[seq_feature.type] = 1
            else:
                counters[seq_feature.type] += 1
            if 'pseudo' in seq_feature.qualifiers:
                counters['pseudo_qualifier'] += 1
        # separate variable for total
        total = 0
        for k, v in counters.iteritems():
            print '\t%s\t%s' % (v, k)
            if k != 'pseudo_qualifier':
                total += v
        print 'Total features:', total


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print '1 argument is required: GenBank input file'
        sys.exit(1)
    else:
        count(sys.argv[1])

