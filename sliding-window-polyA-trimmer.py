#!/usr/bin/python
# encoding: utf-8
'''
Accepts a single FASTQ file as a positional argument.

Use:
- choose window, cut-off, and minimal length options (defaults should be fine, though);
- run the script.

Note: despite starting n_CPUs trimmer workers, this script does not scale beyond ~3-4 CPUs
on my system, possibly because either the reader or the writer worker is a bottleneck.
'''


from __future__ import print_function
import sys
import os

from argparse import ArgumentParser
from multiprocessing import Process, Queue, cpu_count
from Queue import Empty
from Bio.SeqIO.QualityIO import FastqGeneralIterator


def main():
    # TODO: allow input on stdin and output on stdout for piping.
    # TODO: allow multiple input files.
    try:
        parser = ArgumentParser()
        parser.add_argument(dest="fastq", help="path to the FASTQ file", metavar="path")
        # TODO: preserve order, w/o using Pool.map/Pool.imap (they consume whole iterators at the start).
#        parser.add_argument("-o", "--order", dest="order", action="store_true",
#                            default = True,
#                            help="keep original reads order [default: %(default)s]")
#        # -n has to be below -o, otherwise default is not really True (first parsed option sets the variable).
#        parser.add_argument("-n", "--no-order", dest="order", action="store_false",
#                            default = False,
#                            help="do not preserve original reads order [default: %(default)s]")
        parser.add_argument("-m", "--min-length", dest = "min_length", type = int,
                            default = 20,
                            help="discard reads shorter than this [default: %(default)s]")
        parser.add_argument("-w", "--window", type = int, default = 10,
                            help="sliding window size [default: %(default)s]")
        parser.add_argument("-c", "--cutoff", dest = 'cutoff', type = int, default = 1,
                            help="stop trimming if more than this many non-[A|T|N] nucleotides are in the window [default: %(default)s]")
        parser.add_argument("--cpus", type = int, default = cpu_count(),
                            help="use this many CPUs/CPU cores [default: %(default)s]")

        # Process arguments
        args = parser.parse_args()
        print('Window size: %s' % args.window)
        print('Cut-off: %s' % args.cutoff)
        print('Minimal read length: %s' % args.min_length)
        # FIXME: stub
        args.order = False
        # Up to 100 reads queued for each trimming worker.
        qsize = 100 * args.cpus
        # How many As/Ts do we allow within a single window?
        args.allowed = args.window - args.cutoff

        # TODO: Counters.
#        total_reads = 0
#        trimmed_reads = 0
#        short_reads = 0

        # Check if the input file exists; generate output file name.
        if (not os.path.exists(args.fastq)
            or not os.path.isfile(args.fastq)):
            print('There is a problem with the path to the file, exiting.')
            sys.exit(1)
        basename = os.path.splitext(args.fastq)[0]
        outname = '{b}.w{w}c{c}.fastq'.format(b = basename, w = args.window,
                                              c = args.cutoff)

        # Generate A/T oligomers from cut-off/window to start search with.
        polyA = 'A' * args.allowed
        polyT = 'T' * args.allowed

        # Create Queues.
        In = Queue(maxsize=qsize)
        Out = Queue(maxsize=qsize)
        if args.order:
            Serial = Queue(maxsize=qsize)
            Sort = Queue(maxsize=qsize)

        # Window-sliding function.
        def backslide(read, n, pos):
            """
            read: initial read sequence;
            n: nucleotide to look for (A or T, and N);
            pos: rightmost occurrence of the initial search string in the read.
            Returns the rightmost read coordinate for trimming.
            """
            # Slide/loop backwards from (pos - 1).
            for result in range(pos - 1, 0, -1):
                subread = read[result : result + args.window]
                if subread.count(n) + subread.count('N') < args.allowed:
                    # Step one position forward, to avoid trimming a single new non-A|T|N position.
                    return result + 1
            # The entire read is one big poly-tail.
            return 0

        # Parallel workers.
        def reader():
            """
            Read from the FASTQ file, add records to the In queue.
            For sorting, each read gets an integer serial number, which is pushed into the Serial queue.
            Reads are added to In as a tuple (serial, title, seq, qual).
            """
            # TODO: could be one of the bottlenecks of the script, maybe use a naive parser?
            serial = 0
            for title, s, q in FastqGeneralIterator(fastq):
                serial += 1
#                if args.order:
#                    Serial.put(s)
                In.put((serial, title, s, q))
            print('%s reads processed.' % serial)
            for i in range(args.cpus):
                # Tell all trimmers to stop.
                In.put('STOP')

        def trimmer():
            """
            Take reads from the In queue, trim, and send to the Sort queue
            (if keeping order is requested), or to the Out queue otherwise.
            """
            while True:
                try:
                    four_tuple = In.get()
                    if four_tuple == 'STOP':
                        break
                    (serial, title, s, q) = four_tuple
                    # Pre-check length.
                    if len(s) < args.min_length:
                        continue
                    # Find an initial match; should be not further than args.cutoff bp away from the end.
                    # TODO: also enable trimming from the left end.
                    for needle in [polyA, polyT]:
                        # Replace Ns with what we are looking for, for searching only.
                        subststring = s.replace('N', needle[0])
                        pos = subststring.rfind(needle, len(subststring) - args.window)
                        if pos != -1:
                            # Did find a match, now slide the window.
                            trim = backslide(s, needle[0], pos)
                            s = s[ : trim ]
                            q = q[ : trim ]
                            # Either poly-A, or poly-T can be present, not both.
                            break
                    # Post-check length.
                    if len(s) < args.min_length:
                        continue
#                    if args.order:
#                        Sort.put((serial, title, s, q))
#                    else:
                    Out.put((title, s, q))
                except Empty:
                    print('In-queue empty, trimming worker exits.')
                    break

        def sorter():
            """
            Sort reads according to their serial number:
            - has an internal buffer dictionary;
            - reads serials from Serial;
            - for each serial number, iterates through buffer to find one; when found, writes to Out;
            - if not found, iterates Sort queue; when required serial is found, writes to Out, otherwise to the internal buffer.
            """
            buffer = {}
            # FIXME: no proper exit condition.
            while True:
                serial = Serial.get()
                # Look for this serial in the buffer.
                if serial in buffer:
                    Out.put(buffer[serial])
                    del buffer[serial]
                else:
                    # Otherwise, iterate the Sort queue.
                    while not Sort.empty():
                        try:
                            (new_serial, title, s, q) = Sort.get()
                            if new_serial == serial:
                                Out.put((title, s, q))
                                break
                            else:
                                buffer[new_serial] = (title, s, q)
                        except:  # Queue.Empty?
                            continue

        def writer():
            '''Read from Out, write to the output file.'''
            out = open(outname, "w")
            while True:
                try:
                    (title, s, q) = Out.get(timeout = 5)
                    out.write("@%s\n%s\n+\n%s\n" % (title, s, q))
                except Empty:
                    out.close()
                    break

        # Open the files for reading and writing.
        with open(args.fastq) as fastq:
            #print('Starting reader.')
            Process(target = reader).start()
            print('Starting %s trimmers.' % args.cpus)
            for i in range(args.cpus):
                Process(target = trimmer).start()
            if args.order:
                #print('Starting sorter.')
                Process(target = sorter).start()
            #print('Starting writer.')
            writer_worker = Process(target = writer)
            writer_worker.start()
            #print('Waiting for the writer to finish.')
            writer_worker.join()

        # Close Queues.
        In.close()
        Out.close()
        if args.order:
            Serial.close()
            Sort.close()

        # TODO: Print stats: total reads read, reads trimmed, reads discarded, reads written.
        return 0

    except KeyboardInterrupt:
        return 0


if __name__ == "__main__":
    sys.exit(main())
