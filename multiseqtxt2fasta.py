#!/usr/bin/python

import sys

if len(sys.argv) < 2:
    print "Convert text file with 1 sequence per line into a multifasta file with sequential IDs."
    print "Sample ID: 1_2356, where 1 is the number of the sequence in the source file, and 2356 is sequence's length."
    print "Use:", sys.argv[0], "sequences.txt"
else:
    fname = sys.argv[1]
    out = ''
    cnt = 1
    with open(fname) as fhr:
        for line in fhr:
            line = line.strip()
            if line != '':
                out += '>' + str(cnt) + '_' + str(len(line)) + '\n' + line + '\n'
                cnt += 1
    with open(fname, 'w') as fhw:
        fhw.write(out)
