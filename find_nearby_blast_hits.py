#!/usr/bin/env python

from __future__ import print_function
import sys
from collections import defaultdict, OrderedDict

__author__ = 'bogdan'

help_text = "Given 2 CSV files (BLAST format 6, default set of columns), try to find 'significant' hits (as defined by query coverage, evalue, and identity thresholds) which are located nearby (basepairs distance)."

# minimal query coverage
query_cov_threshold = 0.75
# maximal expectation value
evalue_threshold = 1
# minimal percent identity, as percent
identity_threshold = 40.0

# maximal distance between hits
maximal_distance = 1500


def make_row(fields):
    row = OrderedDict()
    row['query_id'] = fields[0]
    row['subj_id'] = fields[1]
    row['identity'] = float(fields[2])
    row['q_length'] = int(fields[3])
    row['mismatches'] = int(fields[4])
    row['gaps_open'] = int(fields[5])
    row['q_start'] = int(fields[6])
    row['q_end'] = int(fields[7])
    row['subj_start'] = int(fields[8])
    row['subj_end'] = int(fields[9])
    row['evalue'] = float(fields[10])
    row['bitscore'] = float(fields[11].strip())
    return row


def passes_thresholds(row, largest_qcov):
    # largest_qcov is the largest observed query coverage
    if (row['q_length'] / largest_qcov >= query_cov_threshold and
                row['evalue'] <= evalue_threshold and
                row['identity'] >= identity_threshold):
        return True
    return False


def print_joint_row(row, row2):
    joint = row.values()
    joint.extend(row2.values())
    joint = map(str, joint)
    print('\t'.join(joint))


def main(first, second):
    # Read first into a dictionary of lists of dictionaries,
    # first = {'genome_id': [row, row, ...]},
    # row = {'field': 'value', ...}
    first_dict = defaultdict(list)
    # second is read into a usual flat list
    second_list = []

    # trackers for the longest query coverage;
    # FIXME: it is assumed to be 100% in at least 1 case
    first_longest_query_coverage = 0.0
    second_longest_query_coverage = 0.0

    with open(first) as handle_1:
        print('Reading', first, file=sys.stderr)
        for line in handle_1:
            print('.', end='', file=sys.stderr)
            row = make_row(line.split('\t'))
            if row['mismatches'] == 0 and row['q_length'] > first_longest_query_coverage:
                first_longest_query_coverage = row['q_length']
            first_dict[row['subj_id']].append(row)
        print(file=sys.stderr)
        print(len(first_dict.keys()), 'rows read', file=sys.stderr)
        #print(first_dict)
        print('Longest query coverage is', first_longest_query_coverage, file=sys.stderr)

    # Read the second in a similar fashion, but into a flat list of rows (we'll iterate this one).
    # FIXME: order of BLAST results matters: second must be the larger/longer of the two.
    with open(second) as handle_2:
        print('Reading', second, file=sys.stderr)
        for line in handle_2:
            print('.', end='', file=sys.stderr)
            row = make_row(line.split('\t'))
            if row['mismatches'] == 0 and row['q_length'] > second_longest_query_coverage:
                second_longest_query_coverage = row['q_length']
            second_list.append(row)
        print(file=sys.stderr)
        print(len(second_list), 'rows read', file=sys.stderr)
        #print(second_list)
        print('Longest query coverage is', second_longest_query_coverage, file=sys.stderr)

    # Iterate the second_list, find matches.
    for row2 in second_list:
        # Does this row pass thresholds?
        if passes_thresholds(row2, second_longest_query_coverage):
            # Yes. Find the corresponding row(s) in first_dict.
            #print('Row', row2, 'passed thresholds')
            if row2['subj_id'] not in first_dict:
                continue
            # else: get a list of hits in the other genome
            found = first_dict[row2['subj_id']]
            # and iterate them,
            for row1 in found:
                #print('found', row1, 'matching', row2)
                # checking it for thresholds, and
                if not passes_thresholds(row1, first_longest_query_coverage):
                    continue
                # checking distance constraint.
                # Note: blast switches start/end to indicate strand, so we check all pairs of start-end
                # coordinates for distances between them.
                if (abs(row1['subj_start'] - row2['subj_start'] <= maximal_distance) or
                        abs(row1['subj_start'] - row2['subj_end'] <= maximal_distance) or
                        abs(row1['subj_end'] - row2['subj_start'] <= maximal_distance) or
                        abs(row1['subj_end'] - row2['subj_end'] <= maximal_distance)):
                    print_joint_row(row1, row2)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Two arguments are expected: blast_result1.csv blast_result2.csv')
        print(help_text)
    else:
        main(sys.argv[1], sys.argv[2])
