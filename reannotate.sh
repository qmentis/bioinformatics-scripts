#!/bin/bash

# Takes all GenBank files (*.gb) in the current directory as input,
# converts to fasta, re-annotates using prokka and antismash2,
# produces NAME_reannotated.gb files.

for name in *.gb;
do
    base=`basename $name .gb`
    fasta="${base}.fna"
    result="${base}_reannotated.gb"
    # optional: extract genus, locustag, species, strain from the original .gb file;
    # not all input files will have this;
    # --locustag ABA --centre AMEG --genus Actinoalloteichus --species spp --strain baikalus --usegenus
    # do provide outdir, otherwise an automatic name will be generated;
    # --gcode 11 and --gram pos are the same for all actinobacteria
    # find prokka's .gbk file, feed it to antismash2
    gb2fasta.py "$name" &&
    prokka --outdir prokka --gcode 11 --gram pos --rfam "$fasta" &&
    rm "$fasta" &&
    mv prokka/*.gbk prokka.gbk &&
    rm -rf prokka &&
    run_antismash --outputfolder as2 --verbose --cpus 8 --input-type nucl --clusterblast --subclusterblast --smcogs --full-hmmer --full-blast prokka.gbk &&
    mv as2/*.final.gbk "$result" &&
    rm -rf as2 &&
    rm prokka.gbk
done

