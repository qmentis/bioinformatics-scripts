#!/usr/bin/python

"""
Explanation of columns.
Feature: type, can only be CDS or ORF; only CDS in S. espanaensis
locus_tag: CDS locus_tag
strand: CDS strand
start: CDS start
end: CDS end
product: CDS product
codon_start: location of the codon first nucleotide relative to CDS 5' end
codon_end: location of the last codon nucleotide relative to CDS 5' end
notes: CDS notes, if any
"""

from __future__ import print_function
import sys
from Bio import SeqIO


def find_TTA(infile):
    """
    Given a GenBank file, look at all CDS, and find TTA codons in them.
    Output a table with these codons, their coordinates, and which genes they were found in.
    :param infile: GenBank flatfile to work with.
    """
    print("Looking for TTA codons in {}.".format(infile), file=sys.stderr)
    print("Feature\tlocus_tag\tstrand\tstart\tend\tproduct\tcodon_start\tcodon_end\tnotes")
    TTA_counter = 0
    with open(infile) as in_handle:
        for record in SeqIO.parse(in_handle, "genbank"):
            print("Processing GenBank record {} ({})".format(record.name, record.id), file=sys.stderr)
            for f in record.features:
                if f.type in ['CDS', 'ORF']:
                    assert len(f) % 3 == 0
                    # Extract feature sequence.
                    seq = f.extract(record.seq)
                    # This code is based on a snippet somewhere from the Internet... But lost the reference...
                    for cindex in range(len(seq) // 3):  # // gets integer part of division result
                        cur_codon = seq[cindex * 3:(cindex + 1) * 3]
                        # if cindex == 0:
                        #     # Show start codons found: for debugging
                        #     print(cur_codon, file=sys.stderr)
                        if cur_codon == 'TTA':
                            TTA_counter += 1
                            # Feature\tlocus_tag\tstrand\tstart\tend\tproduct\tcodon_start\tcodon_end\tnotes
                            locus_tag = '; '.join(f.qualifiers['locus_tag']) if 'locus_tag' in f.qualifiers else ''
                            product = '; '.join(f.qualifiers['product']) if 'product' in f.qualifiers else ''
                            note = '; '.join(f.qualifiers['note']) if 'note' in f.qualifiers else ''
                            print('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}'.format(f.type, locus_tag, f.strand,
                                                                              f.location.start, f.location.end,
                                                                              product, cindex*3 + 1, (cindex + 1) * 3,
                                                                              note))
    print("Found {} TTA codons.".format(TTA_counter), file=sys.stderr)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('1 argument required: GenBank input file')
        sys.exit(2)
    else:
        find_TTA(sys.argv[1])
