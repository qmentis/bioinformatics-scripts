#! /usr/bin/env python
import gzip, os, sys
from Bio import SeqIO
from Bio.SeqIO.QualityIO import FastqGeneralIterator

def grab_nonCCS_reads(filtered_subreads_filename, ccs_filename, output_filename):
    """
    Output sequences that are in filtered_subreads but NOT CCS

    CCS id: m120817_065450_42161_c100377052550000001523029810101262_s1_p0/26/ccs
    or 
    CCS id: m120817_065450_42161_c100377052550000001523029810101262_s1_p0/26

    subread id: m120817_065450_42161_c100377052550000001523029810101262_s2_p0/4/0_337
    """
    # newer versions of filtered_CCS does not have /ccs ! so check!
    ccs_id = gzip.open(ccs_filename, 'rt').readline().strip()
    with gzip.open(ccs_filename, 'rt') as ccs_fh:
        if ccs_id.endswith('/ccs'):
            in_ccs = set(title[:-4] for title, s, q in FastqGeneralIterator(ccs_fh))
        else:
            in_ccs = set(title for title, s, q in FastqGeneralIterator(ccs_fh))

    subreads_total = 0
    subreads_nonccs = 0

    with gzip.open(filtered_subreads_filename, 'rt') as filt_fh, gzip.open(output_filename, 'wt') as out_fh:
        for title, seq, qual in FastqGeneralIterator(filt_fh) :
            subreads_total += 1
            movieName_holeNumber = title[:title.rfind('/')]
            if movieName_holeNumber not in in_ccs:
                subreads_nonccs += 1
                out_fh.write("@%s\n%s\n+\n%s\n" % (title, seq, qual))

    print(ccs_filename)
    print("\tNumber of subreads: {0}".format(subreads_total))
    print("\tNumber of CCS reads: {0}".format(len(in_ccs)))
    print("\tNumber of non-CCS subreads: {0}".format(subreads_nonccs))


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser(description="Grab non-CCS subreads from all_subreads.fastq.gz")
    parser.add_argument(dest="subreads", help="all subreads, filename.fastq.gz")
    parser.add_argument(dest="ccs_filename", help="CCS, filename.ccs.fastq.gz")
    parser.add_argument(dest="output_filename", help="output: subreads not used for CCS construction, filename.fastq.gz")

    args = parser.parse_args()
    grab_nonCCS_reads(args.subreads, args.ccs_filename, args.output_filename)
