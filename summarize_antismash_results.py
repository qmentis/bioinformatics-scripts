#!/usr/bin/python
'''
When started in a directory with multiple sub-directories containing
antismash2 results, dump to stdout (and write to CSV) summary of results:
accession, strain, genome size, number of CDS, number of clusters, taxonomy.

Depends on the get_ncbi_taxonomy.sh script for taxonomy parsing.
'''


from __future__ import print_function
import os
import glob
import prettytable
import csv
import sys
if os.name == 'posix' and sys.version_info[0] < 3:
    import subprocess32 as subprocess
else:
    import subprocess
# TODO: remove, replace with os.*
from sys import argv, exit, stderr
from Bio import SeqIO


if len(argv) != 2:
    print('Required argument is missing, aborting: output_filename.csv',
          file=stderr)
    exit(1)

if os.path.exists(argv[1]):
    print('Output file exists, aborting:', argv[1], file=stderr)
    exit(2)

header = ['primary accession', 'other accessions', 'species', 'genome size',
          'CDS count', 'clusters count', 'phylum', 'class', 'order', 'family',
          'genus']


def get_taxonomy_dict(gi):
    """
    Given a GI, return a dictionary mapping taxonomy ranks (genus, order, ...)
    to their values for the given GI. Set empty ranks to empty strings.
    """
    ranks = ['class', 'family', 'forma', 'genus', 'infraclass', 'infraorder',
             'kingdom', 'no rank', 'order', 'parvorder', 'phylum', 'species',
             'species group', 'species subgroup', 'subclass', 'subfamily',
             'subgenus', 'subkingdom', 'suborder', 'subphylum', 'subspecies',
             'subtribe', 'superclass', 'superfamily', 'superkingdom',
             'superorder', 'superphylum', 'tribe', 'varietas']
    taxa = {}
    # Initialize the final dictionary with empty strings.
    for k in ranks:
        taxa[k] = ''
    try:
        result = subprocess.check_output(['get_ncbi_taxonomy.sh', gi],
                                         stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError:
        print('Problem running the get_ncbi_taxonomy.sh script with GI', gi,
              file=stderr)
        print(result, file=stderr)
        print('Please verify manually that get_ncbi_taxonomy.sh works, then retry.',
              file=stderr)
        print('Note: get_ncbi_taxonomy.sh must be in the PATH.', file=stderr)
        raise
    # result: 'gi\trank:name; ...;'
    pairs = result.strip(';').split('\t')[1].split(';')
    for pair in pairs:
        if len(pair.strip()) == 0:
            continue
        try:
            rank, name = pair.split('::')
        except ValueError:
            print('Failed splitting: "%s"' % pair, file = stderr)
            raise
        taxa[rank] = name
    # Drop the 'no rank'.
    del taxa['no rank']
    return taxa


csv_rows = [header]
stats = prettytable.PrettyTable(header)

for infile in glob.glob(os.path.join('N?_*', '*.final.gbk')):
    print(infile, file=stderr)
    with open(infile) as inhandle:
        genome_size = 0
        CDS_count = 0
        clusters_count = 0
        organism = {}  # maps accessions to 'organism' field
        gi = {}  # maps accessions to GI's
        primary_accession = ''  # for the accession of the longest record
        primary_length = 0  # current primary accession sequence length
        accessions = []  # other accessions, e.g. plasmids
        for r in SeqIO.parse(inhandle, 'genbank'):
            accessions.append(r.name)
            gi[r.name] = r.annotations['gi']
            organism[r.name] = r.annotations['organism']
            if primary_accession == '':
                primary_accession = r.name
                primary_length = len(r.seq)
            elif len(r.seq) > primary_length:
                primary_accession = r.name
                primary_length = len(r.seq)
            genome_size += len(r.seq)
            for f in r.features:
                if f.type == 'CDS':
                    CDS_count += 1
                if f.type == 'cluster':
                    clusters_count += 1
        # Remove primary accession from the list of all accessions.
        accessions.remove(primary_accession)
        accessions = '; '.join(accessions)
        strainstats = [primary_accession, accessions, organism[primary_accession],
                       genome_size, CDS_count, clusters_count]
        # Get taxonomy tree, append it to strain stats.
        taxonomy_dict = get_taxonomy_dict(gi[primary_accession])
        taxonomy = [taxonomy_dict['phylum'], taxonomy_dict['class'],
                    taxonomy_dict['order'], taxonomy_dict['family'],
                    taxonomy_dict['genus']]
        strainstats.extend(taxonomy)
        stats.add_row(strainstats)
        csv_rows.append(strainstats)

csvh = open(argv[1], 'wb')
csv_writer = csv.writer(csvh)
csv_writer.writerows(csv_rows)
csvh.close()

print(stats)
