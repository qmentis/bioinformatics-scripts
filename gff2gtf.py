#!/usr/bin/env python

import csv, sys

input_filename = sys.argv[1]
output_filename = sys.argv[2]


reader = csv.reader(open(input_filename, 'r'), delimiter='\t')
writer = open(output_filename, 'w')

for row in reader:
    anno = dict([[x.split('=')[0], '='.join(x.split('=')[1:])] for x in row[-1].split(';')])
    new_anno = {}
    if 'gene_id' in anno:
        new_anno['gene_id'] = anno['gene_id']
    elif 'Parent' in anno:
        new_anno['gene_id'] = anno['Parent']
    elif 'ID' in anno:
        new_anno['gene_id'] = anno['ID']
    elif 'GeneId' in anno:
        # tRNA
        new_anno['gene_id'] = 'tRNA_'+anno['GeneId']

    print anno
    if 'transcript_id' in anno:
        new_anno['transcript_id'] = anno['transcript_id']
    elif 'gene_id' in new_anno and len(new_anno['gene_id'])>4 and new_anno['gene_id'][-4] == '_' and new_anno['gene_id'][-3] == 'T':
        # maize specific transcripts
        new_anno['transcript_id'] = new_anno['gene_id']
        new_anno['gene_id'] = new_anno['gene_id'][:-4]
    elif 'gene_id' in new_anno:
        new_anno['transcript_id'] = new_anno['gene_id'] + '.1'
    elif 'locus_tag' in new_anno:
        new_anno['transcript_id'] = new_anno['locus_tag'] + '.1'

    if 'protein_id' in anno:
        new_anno['protein_id'] = anno['protein_id']
    elif 'locus_tag' in new_anno:
        new_anno['protein_id'] = anno['locus_tag']

    writer.write('\t'.join(row[:-1]))
    writer.write('\t')
    writer.write('; '.join(['%s \"%s\"'%(x, new_anno[x]) for x in new_anno]))
    writer.write('\n')
