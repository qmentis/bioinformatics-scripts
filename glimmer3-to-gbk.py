#!/usr/bin/env python3

# Based on http://bytesizebio.net/2009/10/29/short-bioinformatics-hacks-glimmer-splitter/
# and csv_annotation_to_genbank.py.

from __future__ import print_function
import sys
import os
from Bio import SeqIO, SeqFeature
from Bio.Alphabet import generic_dna


def predict_to_gbk(glimmer_file, fasta_file):
    """
    Read in Glimnmer3 .predict file and the .fna file, write a GenBank file with all the genes.
    :param glimmer_file: .predict file
    :param fasta_file: .fna file used as input for Glimmer3
    """
    outfilename = os.path.splitext(os.path.basename(glimmer_file))[0] + '.gbk'
    if os.path.exists(outfilename):
        print("Intended output file {} already exists, aborting....".format(outfilename))
        sys.exit(1)

    # Read multi-fasta file records. These will be reused for writing later.
    records_counter = 0
    records = {}
    with open(fasta_file) as in_handle:
        for record in SeqIO.parse(in_handle, "fasta", generic_dna):
            print("FASTA record {}.".format(record.id))
            records_counter += 1
            records[record.id] = record
    print("Parsed and collected {} FASTA record(s).".format(records_counter))

    # Read the glimmer file, record by record
    seqname = ''
    for inline in open(glimmer_file):
        if inline.startswith('>'):
            # First finalize the previous record, if any
            if seqname != '':
                # Note: this code does not run for the last record (and also not for the only/single record).
                print("Record {} contained {} features.".format(seqname, features_counter))
            # Start the new record
            features_counter = 0
            # Glimmer3 copies the identifier verbatim from FASTA, so the first non-space part is an ID.
            seqname = inline.split()[0][1:]
            continue
        elif "orf" not in inline:
            continue
        features_counter += 1
        orfname, start, end, rf, score = inline.strip().split()
        # Prepare the Note string for the Feature.
        qualifiers = {
            'note': "Glimmer3: orfname {}, start {}, end {}, reading frame {}, score {}".format(orfname, start, end, rf,
                                                                                                score)}
        start = int(start)
        end = int(end)
        rf = int(rf)
        # reverse complement
        if rf < 0:
            start, end = end, start
            strand = -1
        else:
            strand = 1
        start -= 1  # Python indexes start a 0
        score = float(score)
        records[seqname].features.append(SeqFeature.SeqFeature(location=SeqFeature.FeatureLocation(start, end),
                                                               type='CDS', strand=strand, qualifiers=qualifiers))
    # Print data for the last processed record.
    print("Record {} contained {} features.".format(seqname, features_counter))
    SeqIO.write(list(records.values()), open(outfilename, "w"), "genbank")


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print(
            '2 arguments are required: something.predict something-else.fasta ; output will be written to something.gbk')
        sys.exit(1)
    else:
        predict_to_gbk(sys.argv[1], sys.argv[2])
