#!/usr/bin/python

import sys
from os.path import exists, isdir, isfile, join, basename, getsize
from shutil import copy


def copy_overwriting_smaller_files(source, destination):
    '''
    source is the path to the source file
    destination is the path to destination directory (it must exist)
    '''
    # 1. Check that source exists and is a file.
    if exists(source) and isfile(source):
        # 2. Check that destination exists and is a directory.
        if exists(destination) and isdir(destination):
            # 3. Check if there is a file with the same name in destination.
            target = join(destination, basename(source))
            if not exists(target):
                # simply copy
                print '    copying', source
                copy(source, target)
            else:
                # get both file sizes
                source_size = getsize(source)
                target_size = getsize(target)
                if source_size > target_size:
                    # overwrite
                    print 'overwriting', basename(source), '(%s)' % target_size, 'with (%s)' % source_size
                    copy(source, target)
                else:
                    # skip, silently
                    pass
        else:
            print 'destination', destination, 'does not exist or is not a directory'
    else:
        print 'source', source, 'does not exist or is not a file'


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print 'Two arguments are required, in this order: destination_directory source_file'
        print 'This script will:'
        print '- check if the target file exists: if not, copy source file to destination directory;'
        print '- if exists, and is smaller than source: overwrite, printing file name and sizes to stdout;'
        print '- if exists, and is larger than source: skip (do not overwrite).'
        sys.exit(2)
    else:
        destination = sys.argv[1]
        source = sys.argv[2]
        copy_overwriting_smaller_files(source, destination)
