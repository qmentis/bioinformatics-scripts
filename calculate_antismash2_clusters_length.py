#!/usr/bin/python

"""
Given a GenBank file output from antismash2 and a list of cluster types to exclude,
calculate and print:
- total number of cluster (w/o excluded types),
- total length (in basepairs) of all non-excluded clusters,
- average cluster length (w/o excluded cluster types).

To get these numbers for all clusters, simply do not provide the list of excluded
cluster types.
"""


import sys
from Bio import SeqIO


def count_lengths(path, excluded):
    counter = 0
    tlen = 0.0
    for rec in SeqIO.parse(path, format="genbank"):
        for f in rec.features:
            # antismash2 uses 'cluster' feature type, and stores cluster type
            # in the 'product' qualifier.
            if f.type == 'cluster' and f.qualifiers['product'][0] not in excluded:
                counter += 1
                tlen += (f.location.end - f.location.start)
    print 'clusters:', counter
    print 'total clusters length, bp:', int(tlen)
    print 'average cluster length, bp:', round(tlen/counter, 2)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print
        print 'At least 1 argument is required: path to antismash2 GenBank file.'
        print 'This must be the first argument, which can be (optionally)'
        print 'followed by the names of cluster types you would like to exclude'
        print 'from calculations.'
        print
        print 'Example command line 1: get stats for all clusters from the file:'
        print '$', sys.argv[0], 'path_to_antismash2_output.gbk'
        print
        print 'Example command line 2: exclude "putative" and "other" cluster types from statistics:'
        print '$', sys.argv[0], 'path_to_antismash2_output.gbk', 'putative other'
        print
        sys.exit(1)
    elif len(sys.argv) == 2:
        # no cluster types to exclude
        count_lengths(sys.argv[1], [])
    else:
        # there are some cluster types to exclude
        count_lengths(sys.argv[1], sys.argv[2:])

