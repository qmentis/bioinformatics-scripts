#!/usr/bin/python

"""
Read multi-fasta file, then read annofile.csv line by line.
Add found features to the corresponding FASTA records.
Write a multi-record genbank file.
"""

from __future__ import print_function
import sys
from Bio import SeqIO, SeqFeature
from Bio.Alphabet import generic_dna


def overlay_csv_on_fasta(fastafile, annofile, outfile):
    """
    :param fastafile: name of the input multi-fasta file
    :param annofile: name of the annotations CSV file; see below for file format example; columns are tab-separated:
        contig	start	end	strand	type	name	product	EC_code
        1	86	532	-	CDS	-	hypothetical protein	-
        1	696	1070	-	CDS	atpC	ATP synthase epsilon chain	-
        1	1180	2622	-	CDS	atpD	ATP synthase subunit beta	3.6.3.14
        1	2622	3539	-	CDS	atpG	ATP synthase gamma chain	-
    :return: no return value
    """

    # Read multi-fasta file records.
    records_counter = 0
    records = {}
    with open(fastafile) as in_handle:
        for record in SeqIO.parse(in_handle, "fasta", generic_dna):
            # print("GenBank record {}".format(record.id))
            records_counter += 1
            records[record.id] = record
    print("Parsed and collected {} FASTA records.".format(records_counter))

    # Read annotations file.
    features_counter = 0
    header_seen = False
    with open(annofile) as annohandle:
        for line in annohandle:
            if not header_seen:
                header_seen = True
                continue
            # else: process the line
            features_counter += 1
            contig, start, end, strand, ftype, gene, product, EC_code = line.strip().split('\t')
            if strand == '+':
                strand = 1
            elif strand == '-':
                strand = -1
            else:
                # '.'
                strand = None
            qualifiers = {'product': [product]}
            if EC_code != '-':
                qualifiers['EC_number'] = [EC_code]
            if gene != '-':
                qualifiers['gene'] = [gene]
            # qualifiers['locus_tag'] = [?]
            # id = ?
            feature = SeqFeature.SeqFeature(location=SeqFeature.FeatureLocation(int(start) - 1, int(end)),
                                            type=ftype, strand=strand, qualifiers=qualifiers)
            records[contig].features.append(feature)
    print('Processed {} features.'.format(features_counter))

    # Write GenBank.
    with (open(outfile, 'w')) as outhandle:
        SeqIO.write(records.values(), outhandle, "genbank")


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('3 arguments required: multifasta.fna annotation.csv outfile.genbank')
        sys.exit(1)
    else:
        overlay_csv_on_fasta(sys.argv[1], sys.argv[2], sys.argv[3])
