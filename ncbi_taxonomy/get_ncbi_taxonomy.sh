#!/bin/bash

# Author: Frédéric Mahé
# Source: http://www.biostars.org/p/13452/#13648
# Sample use (for blast results): cut -d "|" -f 2 myblast.table | sed -e '/^$/d' | grep -v "^#" | while read GI ; do bash get_ncbi_taxonomy.sh "$GI" ; done
# To parallelize: xargs --arg-file=GI.list --max-procs=8 -I '{}' bash get_ncbi_taxonomy.sh '{}'


# Edit this path! It must point to the directory where the download_taxonomy.sh was executed. Must end with a slash.
PATH_TO_DATA="/home/bogdan/taxonomy/"

NAMES="names.dmp"
NODES="nodes.dmp"
GI_TO_TAXID="gi_taxid_nucl.dmp"
TAXONOMY=""
GI="${1}"

# Obtain the name corresponding to a taxid or the taxid of the parent taxa
function get_name_or_taxid() {
    grep --max-count=1 "^${1}"$'\t' "${2}${3}" | cut --fields="${4}"
}

# Get the taxid corresponding to the GI number
TAXID=$(get_name_or_taxid "${GI}" "${PATH_TO_DATA}" "${GI_TO_TAXID}" "2")

# Loop until you reach the root of the taxonomy (i.e. taxid = 1)
while [[ "${TAXID}" -gt 1 ]]; do
    # Obtain the scientific name corresponding to a taxid
    NAME=$(get_name_or_taxid "${TAXID}" "${PATH_TO_DATA}" "${NAMES}" "3")
    # Obtain the parent taxa taxid
    PARENT=$(get_name_or_taxid "${TAXID}" "${PATH_TO_DATA}" "${NODES}" "3")
    # Obtain taxa rank name (phylum, genus, ...)
    RANK=$(get_name_or_taxid "${TAXID}" "${PATH_TO_DATA}" "${NODES}" "5")
    # Build the taxonomy path
    #TAXONOMY=$(echo "${RANK}::${NAME};${TAXONOMY}")
    TAXONOMY="${NAME};${TAXONOMY}"
    TAXID="${PARENT}"
done

echo -e "${GI}\t${TAXONOMY}"

exit 0
