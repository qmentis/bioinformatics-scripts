#!/usr/bin/python
"""
    Filter the provided protein multifasta file (*.faa) using the list of allowed
    protein IDs (one ID per line, text file). Print filtered multifasta as output.
"""


import sys
from Bio import SeqIO


def filter_multifasta(faa, allowed):
    # 1. Read allowed IDs into memory.
    with open(allowed) as fh:
        allowed_set = set([line.strip() for line in fh.readlines()])
    # 2. Iterate through multifasta, only print allowed records.
    with open(faa) as faah:
        for r in SeqIO.parse(faah, "fasta"):
            if r.id in allowed_set:
                SeqIO.write(r, sys.stdout, "fasta")


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('2 arguments are required: multifasta.faa IDs.txt')
        sys.exit(2)
    else:
        filter_multifasta(sys.argv[1], sys.argv[2])
