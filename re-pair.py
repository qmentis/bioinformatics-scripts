#!/usr/bin/python

"""
@author Peter Cock
http://seqanswers.com/forums/showthread.php?t=6140

With minor convenience modifications.
Also: modified to closer match re-pair_interleaved.py.
"""


import sys
from Bio import SeqIO  # Biopython 1.54 or later needed


def re_pair(args):
    # Expected order of arguments: in1, in2, out1, out2, out-singles.
    input_forward_filename = args[0]
    input_reverse_filename = args[1]
    output_paired_forward_filename = args[2]
    output_paired_reverse_filename = args[3]
    output_orphan_filename = args[4]

    # Most common suffix (e.g. from vendor 'BC').
    # TODO: add test with these suffixes.
    f_suffix = "/1"
    r_suffix = "/2"
    # Less common suffix (e.g. from head office B. sequencing).
    # TODO: add test with these suffixes.
    # TODO: as these are space-separated from the name, they do not need to
    # be declared/known at all. The only thing needed is a detect_suffix()
    # method, which would return either None (if pre-space part of the name
    # has no suffix) or a tuple of suffixes (/1, /2) if they exist in the name.
    alt_f_suffix = ' 1:N:0:1'
    alt_r_suffix = ' 2:N:0:1'

    if f_suffix:
        f_suffix_crop = -len(f_suffix)
        def f_name(title):
            """Remove the suffix from a forward read name."""
            name = title.split()[0]
            assert name.endswith(f_suffix), name
            return name[:f_suffix_crop]
    else:
        def f_name(title):
            return title.split()[0]

    if r_suffix:
        r_suffix_crop = -len(r_suffix)
        def r_name(title):
            """Remove the suffix from a reverse read name."""
            name = title.split()[0]
            assert name.endswith(r_suffix), name
            return name[:r_suffix_crop]
    else:
        def r_name(title):
            return title.split()[0]

    print "Indexing forward file..."
    forward_dict = SeqIO.index(input_forward_filename, "fastq", key_function=f_name)

    print "Indexing reverse file..."
    reverse_dict = SeqIO.index(input_reverse_filename, "fastq", key_function=r_name)

    print "Outputting proper pairs and forward orphans..."
    forward_handle = open(output_paired_forward_filename, "w")
    reverse_handle = open(output_paired_reverse_filename, "w")
    orphan_handle = open(output_orphan_filename, "w")

    for key in forward_dict:
        if key in reverse_dict:
             forward_handle.write(forward_dict.get_raw(key))
             reverse_handle.write(reverse_dict.get_raw(key))
        else:
             orphan_handle.write(forward_dict.get_raw(key))
    forward_handle.close()
    reverse_handle.close()

    print "Outputting reverse orphans..."
    for key in reverse_dict:
        if key not in forward_dict:
             orphan_handle.write(reverse_dict.get_raw(key))
    orphan_handle.close()
    print "Done"


if __name__ == '__main__':
    if len(sys.argv) != 6:
        print "This script requires 6 positional arguments:"
        print "\t- forward/1st pair FASTQ filename"
        print "\t- reverse/2nd pair FASTQ filename"
        print "\t- output filename for the 1st read from the pair"
        print "\t- output filename for the 2nd read from the pair"
        print "\t- output filename for single reads"
        sys.exit(1)
    else:
        re_pair(sys.argv[1:])

