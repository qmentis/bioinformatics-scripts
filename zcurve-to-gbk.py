#!/usr/bin/env python3

from __future__ import print_function
import sys
import os
from Bio import SeqIO, SeqFeature
from Bio.Alphabet import generic_dna


def zcurve_to_gbk(zcurve_file, fasta_file):
    """
    Read in zcurve output file and the .fna file, write a GenBank file with all the genes.
    :param zcurve_file: main zcurve output file
    :param fasta_file: .fna file used as input for zcurve
    """
    outfilename = os.path.splitext(os.path.basename(zcurve_file))[0] + '.gbk'
    if os.path.exists(outfilename):
        print("Intended output file {} already exists, aborting....".format(outfilename))
        sys.exit(1)

    # ZCURVE on a single record produces a header like:
    # >An anonymous input sequence
    # which suggests that it cannot handle multi-fasta input files...
    records_counter = 0
    records = {}
    with open(fasta_file) as in_handle:
        for record in SeqIO.parse(in_handle, "fasta", generic_dna):
            print("FASTA record {}.".format(record.id))
            records_counter += 1
            records[record.id] = record
    print("Parsed and collected {} FASTA record(s).".format(records_counter))
    if records_counter > 1:
        print('WARNING: zcurve seems to have no support for multifasta files, and this *is* a multi-fasta.')
        print('This script will only process the first record from both input files. You have been warned!')

    # Read zcurve file, record by record
    features_counter = 0
    for inline in open(zcurve_file):
        inline_stripped = inline.strip()
        if inline_stripped.startswith('>') or inline_stripped == '' or inline_stripped.startswith('No'):
            # header, empty lines, column headers
            continue
        features_counter += 1
        orfnum, start, end, strand, length, z_index = inline.strip().split()
        # Prepare the Note string for the Feature.
        qualifiers = {
            'note': "ZCURVE3.0: orfnum {}, start {}, end {}, strand {}, length, z_index {}".format(orfnum, start, end,
                                                                                                   strand, length,
                                                                                                   z_index)}
        start = int(start)
        end = int(end)
        if strand == '+':
            strand = 1
        else:
            strand = -1
        start -= 1  # Python indexes start a 0
        # TODO: fix fragility, disable multifasta reading.
        records[list(records.keys())[0]].features.append(
            SeqFeature.SeqFeature(location=SeqFeature.FeatureLocation(start, end),
                                  type='CDS', strand=strand, qualifiers=qualifiers))
    # Print data for the last processed record.
    print("Record {} contained {} features.".format(list(records.keys())[0], features_counter))
    SeqIO.write(records.values(), open(outfilename, "w"), "genbank")


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print(
            '2 arguments are required: zcurve.out something.fasta ; output will be written to zcurve.gbk')
        sys.exit(1)
    else:
        zcurve_to_gbk(sys.argv[1], sys.argv[2])
