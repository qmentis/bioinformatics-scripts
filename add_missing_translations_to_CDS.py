#!/usr/bin/python

from __future__ import print_function
import sys
from os.path import splitext, exists
from Bio import SeqIO

__author__ = 'bogdan'

# Given a GenBank input file, look for CDS/ORFs which do NOT have 'translation' qualifier,
# and add it. Write a new genbank outfile. Skip pseudo-genes by default.
# Translation uses Bacterial table 11.


def add_missing_translations(infile, outfile='', skip_pseudo=True):
    """
    Given a GenBank file, look at all CDS, and extract their 'translation'
    qualifier, if exists; otherwise translate using Bacterial table 11.
    Do not extract/translate CDS with 'pseudo' attribute.
    Resulting amino-acid multifasta file will have identifiers of the form:
    (locus_tag:protein_id|locus_tag|gene|protein_id|label):sequence_record.id
    """
    if outfile == '':
        outfile = splitext(infile)[0] + '.with-missing-translations' + splitext(infile)[1]
    if exists(outfile):
        print('%s exists, not overwriting!' % outfile, file=sys.stderr)
    else:
        with open(infile) as in_handle, open(outfile, "w") as out_handle:
            records = []
            for seq_record in SeqIO.parse(in_handle, "genbank"):
                print("Processing GenBank record %s (%s)" % (seq_record.name, seq_record.id))
                features = []
                for seq_feature in seq_record.features:
                    if seq_feature.type in ['CDS', 'ORF']:
                        if skip_pseudo and 'pseudo' in seq_feature.qualifiers:
                            print("\tSkipping pseudo %s." % seq_feature.qualifiers['locus_tag'][0], file=sys.stderr)
                        elif 'translation' not in seq_feature.qualifiers:
                            print("\tTranslating %s." % seq_feature.qualifiers['locus_tag'][0], file=sys.stderr)
                            seq_feature.qualifiers['translation'] = seq_feature.extract(seq_record.seq).translate(table="Bacterial", cds=True, to_stop=True)
                    features.append(seq_feature)
                seq_record.features = features
                records.append(seq_record)
            # Write new_record to the outfile.
            SeqIO.write(records, outfile, "genbank")


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('1 argument required: GenBank input file')
        sys.exit(2)
    else:
        add_missing_translations(sys.argv[1])
