download_taxonomy.sh and get_ncbi_taxonomy.sh are from the BioStars thread http://www.biostars.org/p/13452/
NCBI_tree_of_life.py is a sample script utilizing the E.T.E. toolkit; it needs names.dmp and nodes.dmp.

This ncbi_taxonomy script is deprecated, now using ete2 toolkit instead.
