#!/usr/bin/python

"""
@author Peter Cock
http://seqanswers.com/forums/showthread.php?t=6140

Unmodified.
"""

from Bio import SeqIO #Biopython 1.54 or later needed

#######################################################
#
# Change the following settings to suit your needs
#

input_forward_filename = "SRR001666_1_rnd.fastq"
input_reverse_filename = "SRR001666_2_rnd.fastq"

output_pairs_filename = "out_interleaved_pairs.fastq"
output_orphan_filename = "out_unpaired_orphans.fastq"

f_suffix = "/1"
r_suffix = "/2"

#######################################################

if f_suffix:
    f_suffix_crop = -len(f_suffix)
    def f_name(name):
        """Remove the suffix from a forward read name."""
        assert name.endswith(f_suffix), name
        return name[:f_suffix_crop]
else:
    #No suffix, don't need a function to fix the name
    f_name = None

if r_suffix:
    r_suffix_crop = -len(r_suffix)
    def r_name(name):
        """Remove the suffix from a reverse read name."""
        assert name.endswith(r_suffix), name
        return name[:r_suffix_crop]
else:
    #No suffix, don't need a function to fix the name
    r_name = None

print "Indexing forward file..."
forward_dict = SeqIO.index(input_forward_filename, "fastq", key_function=f_name)

print "Indexing reverse file..."
reverse_dict = SeqIO.index(input_reverse_filename, "fastq", key_function=r_name)

print "Outputting pairs and forward orphans..."
pair_handle = open(output_pairs_filename, "w")
orphan_handle = open(output_orphan_filename, "w")
for key in forward_dict:
    if key in reverse_dict:
         pair_handle.write(forward_dict.get_raw(key))
         pair_handle.write(reverse_dict.get_raw(key))
    else:
         orphan_handle.write(forward_dict.get_raw(key))
pair_handle.close()
print "Outputting reverse orphans..."
for key in reverse_dict:
    if key not in forward_dict:
         orphan_handle.write(reverse_dict.get_raw(key))
orphan_handle.close()
print "Done"
