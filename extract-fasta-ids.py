#!/usr/bin/env python

"""
%prog some.fasta wanted-list.txt
"""

from __future__ import print_function
from Bio import SeqIO
import sys

if len(sys.argv) < 2:
    print('%prog input.fasta wanted-ids-list.txt > output.fasta')
    sys.exit(1)

wanted = [line.strip() for line in open(sys.argv[2])]
seqiter = SeqIO.parse(open(sys.argv[1]), 'fasta')
SeqIO.write((seq for seq in seqiter if seq.id in wanted), sys.stdout, "fasta")

