#!/usr/bin/env python

"""
fasta2gb
Can be used both as a standalone program and an importable module.
Does not overwrite existing output files.
"""


from __future__ import print_function
import sys
from os.path import exists, splitext
from Bio import SeqIO
from Bio.Alphabet import generic_dna


def fasta2gb(infile, outfile='', static_id=''):
    """
    :param infile: path to (or handle of) the input fasta file
    :param outfile: a name or a path to or a handle of the output file
    :param static_id: record.id to use for all generated/written SeqRecords
    :return: outfile
    """
    if outfile == '':
        outfile = splitext(infile)[0] + '.gbk'
    if exists(outfile):
        print('%s exists, not overwriting!' % outfile, file=sys.stderr)
    else:
        if static_id == '':
            SeqIO.convert(infile, "fasta", outfile, "genbank", generic_dna)
        else:
            # convert record-by-record, changing record.id to static_id
            records = []
            for r in SeqIO.parse(infile, "fasta", generic_dna):
                r.id = static_id
                records.append(r)
            # TODO: may need to manually open outfile for writing in "append" mode.
            SeqIO.write(records, outfile, "genbank")
    return outfile


if __name__ == '__main__':
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        print('1 argument is required: input_file.fasta')
        print('Optionally, you can specify 2nd argument: it will be used as the identifier line in the Genbank.')
        print('Output file will be written to the same filename but with the .gbk extension.')
        sys.exit(2)
    elif len(sys.argv) == 2:
        fasta2gb(sys.argv[1])
    elif len(sys.argv) == 3:
        fasta2gb(sys.argv[1], outfile='', static_id=sys.argv[2])

