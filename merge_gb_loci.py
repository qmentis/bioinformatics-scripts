#!/usr/bin/python
# Given a single multi-LOCUS GenBank, create a new GenBank file
# with all loci artificially merged into a single record.
# Merge order is defined by SeqIO.parse order, most likely
# the same as loci order in the original file.

import sys
from Bio import SeqIO


# FIXME: the resulting GenBank file loses any kind of IDs/versions/identifiers
def merge(infile, outfile, spacer_size):
    # Create spacer string and open files.
    spacer = 'N' * int(spacer_size)
    in_handle = open(infile)
    out_handle = open(outfile, "w")

    is_first = True # let the 1st record be the basis for merging
    merged = None # will hold the final merged record
    for seq_record in SeqIO.parse(in_handle, "genbank"):
        print "GenBank record %s" % seq_record.id
        if is_first:
            is_first = False
            merged = seq_record
            continue
        merged = merged + spacer + seq_record

    # Write out merged loci and close files.
    SeqIO.write(merged, out_handle, "genbank")
    out_handle.close()
    in_handle.close()


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print '3 arguments required: infile, outfile, spacer size'
        sys.exit(1)
    else:
        merge(sys.argv[1], sys.argv[2], sys.argv[3])
