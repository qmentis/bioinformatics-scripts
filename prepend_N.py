#!/usr/bin/python

'''
Right-align sequences in a multi-fasta file by left-appending Ns until the
desired total line length is reached.
'''

import sys

if len(sys.argv) != 3:
    print "Arguments required: fasta filename, desired length of the resulting sequence lines."
    print "Output goes to stdout."
    sys.exit(1)

for line in open(sys.argv[1], 'r'):
    # +1 for trailing newline
    if line.startswith('>') or len(line) == int(sys.argv[2]) + 1:
        print line,
    else:
        # +1 for the trailing newline
        print line.rjust(int(sys.argv[2]) + 1, 'N'),
