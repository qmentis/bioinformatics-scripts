#!/usr/bin/env python

import sys
from BCBio import GFF
from Bio import SeqIO


def convert(infile, outfile):
    in_handle = open(infile)
    out_handle = open(outfile, "w")
    GFF.write(SeqIO.parse(in_handle, "genbank"), out_handle)
    in_handle.close()
    out_handle.close()


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('2 arguments required: infile, outfile')
        sys.exit(1)
    else:
        convert(sys.argv[1], sys.argv[2])
