#!/usr/bin/env python3

"""
gb2fasta
Can be used both as a standalone program and an importable module.
Does not overwrite existing output files.

Output fasta records are generated as follows:
>record.id record.description
"""


from __future__ import print_function
import sys
from os.path import exists, splitext
from Bio import SeqIO


def gb2fasta(infile, outfile='', static_id=''):
    """
    :param infile: path to (or handle of) the input genbank file
    :param outfile: a name or a path to or a handle of the output file
    :param static_id: record.id to use for all generated/written SeqRecords
    :return: outfile
    """
    if outfile == '':
        outfile = splitext(infile)[0] + '.fna'
    if exists(outfile):
        print('%s exists, not overwriting!' % outfile, file=sys.stderr)
    else:
        if static_id == '':
            SeqIO.convert(infile, "genbank", outfile, "fasta")
        else:
            # convert record-by-record, changing record.id to static_id
            records = []
            for r in SeqIO.parse(infile, "genbank"):
                r.id = static_id
                records.append(r)
            # TODO: may need to manually open outfile for writing in "append" mode.
            SeqIO.write(records, outfile, "fasta")
    return outfile


if __name__ == '__main__':
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        print('1 argument is required: input_file.gb')
        print('Optionally, you can specify 2nd argument: it will be used as the identifier line in the FASTA.')
        print('Output file will be written to the same filename but with the .fna extension.')
        sys.exit(2)
    elif len(sys.argv) == 2:
        gb2fasta(sys.argv[1])
    elif len(sys.argv) == 3:
        gb2fasta(sys.argv[1], outfile='', static_id=sys.argv[2])
