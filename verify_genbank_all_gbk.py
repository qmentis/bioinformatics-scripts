#!/usr/bin/python
'''
When started with a path to a directory extracted from GenBank's all.gbk.tar.gz
file of microbial genomes, with multiple sub-directories containing individual
accession genbank files, will check if every directory only contains records
for a single species. If a directory is found which contains genbank files
for 2 or more different species, an output similar to below will be printed:
Vibrio parahaemolyticus O1 directory name
  recordname   organism, record description
  NC_021848 Vibrio parahaemolyticus O1:K33 str. CDC_K4557, Vibrio parahaemolyticus O1:K33 str. CDC_K4557 chromosome I, complete sequence.
  NC_021824 Listeria monocytogenes, Listeria monocytogenes strain J2-064, complete genome.
'''


from __future__ import print_function
from os import listdir
from glob import glob
from os.path import join, isdir, abspath
from sys import argv, exit
from Bio import SeqIO


def scan(dirpath):
    'dirpath: to the expanded all.gbk.tar.gz directory'
    dirpath = abspath(dirpath)
    total_dirs = 0
    total_files = 0
    total_records = 0
    # 1. Get the list of directories under dirpath.
    dirs = [join(dirpath, o) for o in listdir(dirpath) if isdir(join(dirpath, o))]
    # 2. Enter each one.
    for d in dirs:
        total_dirs += 1
        gbks = glob( join(d, '*.gbk') )
        # With only one genbank file, there is nothing to check.
        if len(gbks) < 1:
            print(d, 'does not have any *.gbk files at all!')
        if len(gbks) == 1:
            total_files += 1
            continue
        # Else: there are 2 or more .gbk files.
        mapping = []
        species = []
        for infile in gbks:
            total_files += 1
            for r in SeqIO.parse(infile, 'genbank'):
                total_records += 1
                mapping.append((r.name, r.annotations['organism'], r.description))
                species.append(r.annotations['organism'])
        if len(set(species)) != 1:
            # There must be 2 or more species, print this
            print(d)
            for n, s, d in mapping:
                print('  %s %s, %s' % (n, s, d))
        # Else: just one organism in all files and records.
    print()
    print('Directories:', total_dirs)
    print('Files:', total_files)
    print('Records:', total_records)


if __name__ == '__main__':
    if len(argv) != 2:
        print('Path to all.gbk directory is required.')
        print('run `head', argv[0], '` for more information')
        exit(1)
    else:
        scan(argv[1])
