#!/usr/bin/python

"""
Find TT nucleotides within CDS.
Overlapping TTs are found as well, so 'TTT' has 2 matches.
"""

from __future__ import print_function
import sys
from Bio import SeqIO


def find_TT(infile):
    """
    Given a GenBank file, look at all CDS, and find 'TT' nucleotide pairs in them.
    Outputs:
        - stderr: print CDS identifiers w/o any TTs
        - stderr: print total TTs found in the genome
        - stdout: print a table, "identifier \t number_of_TTs"
    :param infile: GenBank flatfile to work with.
    """
    print("Looking for TT nucleotides in {}.".format(infile), file=sys.stderr)
    print('locus_tag\tTTs found')
    TT_counter = 0
    list_of_CDS_wo_TTs = []
    with open(infile) as in_handle:
        for record in SeqIO.parse(in_handle, "genbank"):
            print("Processing GenBank record {} ({})".format(record.name, record.id), file=sys.stderr)
            for f in record.features:
                if f.type in ['CDS', 'ORF']:
                    f_TT_counter = 0
                    assert len(f) % 3 == 0
                    # Extract feature sequence.
                    seq = f.extract(record.seq)
                    # for needle in ['AA', 'TT']:
                    start = seq.find('TT')
                    while start != -1:
                        f_TT_counter += 1
                        start = seq.find('TT', start + 1)
                    if f_TT_counter == 0:
                        # Not found at all.
                        list_of_CDS_wo_TTs.append(f.qualifiers['locus_tag'][0])
                    else:
                        TT_counter += f_TT_counter
                        print('{}\t{}'.format(f.qualifiers['locus_tag'][0], f_TT_counter))
    print('Total TTs found: {}'.format(TT_counter), file=sys.stderr)
    print('List of CDS without TT: {}'.format(', '.join(list_of_CDS_wo_TTs)), file=sys.stderr)

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('1 argument required: GenBank input file')
        sys.exit(2)
    else:
        find_TT(sys.argv[1])
